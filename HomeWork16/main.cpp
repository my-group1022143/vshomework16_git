#include <iostream>
#include <time.h>
constexpr auto __STDC_WANT_LIB_EXT1__ = 1;

int main()
{
	const int A = 6;
	int array[A][A];
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	int currentDay = buf.tm_mday;
	int sum = 0;
	for (int i = 0; i < A; i++)
	{
		for (int j = 0; j < A; j++)
		{
			array[i][j] = i + j;
		}
	}
	for (int i = 0; i < A; i++)
	{
		for (int j = 0; j < A; j++)
		{
			std::cout.width(2);
			std::cout << array[i][j] << " ";
		}
		std::cout << std::endl;
	}
	for (int i = 0; i < A; i++)
	{
		sum = 0;
		if (currentDay % A != i)
			continue;
		for (int j = 0; j < A; j++)
			sum += array[i][j];
		std::cout << i << ": " << sum << std::endl;
	}
	std::cout << std::endl;
}
